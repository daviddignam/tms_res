# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import UserError, AccessError

import logging
_logger = logging.getLogger(__name__)



class tmsLanguages(models.Model):
	_name = 'tms.languages'
	_description = 'Translation Service Languages'
	
	name = fields.Char()
	code = fields.Char(string='ISO 639-3 Language Code')
	code_ref = fields.Char(string="ISO 639-3 Reference List", default="https://en.wikipedia.org/wiki/Wikipedia:WikiProject_Languages/List_of_ISO_639-3_language_codes_(2019)", help="Use this reference when entering the language code above")
	group = fields.Many2one(comodel_name='tms.language_group',string='Language Rate Group', help="Applies the default rates from the selected Rate group as the language rates. If you wish to have specific rates for a language select the 'Override Rate Group Defaults for this Language' option and enter your custom rates")
	#override settings if override_rate_group display rate fields
	override_rate_group = fields.Boolean(string='Use Custom Rates', help='Override Rate Group Defaults for this Language')
	trans_word = fields.Float(string='Translation Rate per Word')
	trans_page = fields.Float(string='Translation Rate per Page')
	trans_flat = fields.Float(string='Translation Rate Flat Fee')
	proof_flat = fields.Float(string='Proofread Rate Flat Fee')
	proof_word = fields.Float(string='Proofread Rate per Word')
	trans_cost = fields.Float(string='Translation Cost per Word')
	trans_cost_flat = fields.Float(string='Translation Cost Flat Fee')
	proof_cost = fields.Float(string='Proofread Cost per Word')
	proof_cost_flat = fields.Float(string='Proofread Cost Flat Fee')
	
	@api.onchange('group')
	def update_rate(self):
		try:
			if self.group.exists():
				data = self.update_rate_values(self.group)
				self.update(data)
		except Exception:
			_logger.error("Error updating rates based on group", exc_info=True)
			
	def update_rate_values(self, group):
		data = {
			'trans_word': group.trans_word,
			'trans_page': group.trans_page,
			'trans_flat': group.trans_flat,
			'proof_flat': group.proof_flat,
			'proof_word': group.proof_word,
			'trans_cost': group.trans_cost,
			'trans_cost_flat': group.trans_cost_flat,
			'proof_cost': group.proof_cost,
			'proof_cost_flat': group.proof_cost_flat,
		}
		return data
	
	@api.multi
	def write(self, values):
		try:
			_logger.debug("Languages Write: " + str(values))
			if values.get('group'):
				group = self.env['tms.language_group'].search([('id','=',values['group'])])
				data = self.update_rate_values(group)
				values.update(data)
			res = super(tmsLanguages, self).write(values)
			return res
		except Exception:
			_logger.error("Error writing", exc_info=True)
	
	def get_unit_price(self, product_uom, translation_service_type):
		unit_price = 0
		uom_categ_name = product_uom.category_id.name
		if translation_service_type == 'combined':
			unit_price = self.trans_flat
		elif translation_service_type == 'translation':
			if uom_categ_name == 'Flat':
				unit_price = self.trans_flat
			elif uom_categ_name == 'Page':
				unit_price = self.trans_page
			else:
				unit_price = self.trans_word
		elif translation_service_type == 'proofread':
			if uom_categ_name == 'Flat':
				unit_price = self.proof_flat
			elif uom_categ_name == 'Page':
				unit_price = self.proof_flat
			else:
				unit_price = self.proof_word
		return unit_price
	
	
class tmsLanguageGroup(models.Model):
	_name = 'tms.language_group'
	_description = 'Rate groups for Translation Service Languages'
	
	name = fields.Char()
	
	trans_word = fields.Float(string='Translation Rate per Word')
	trans_page = fields.Float(string='Translation Rate per Page')
	trans_flat = fields.Float(string='Translation Rate Flat Fee')
	proof_flat = fields.Float(string='Proofread Rate Flat Fee')
	proof_word = fields.Float(string='Proofread Rate per Word')
	trans_cost = fields.Float(string='Translation Cost per Word')
	trans_cost_flat = fields.Float(string='Translation Cost Flat Fee')
	proof_cost = fields.Float(string='Proofread Cost per Word')
	proof_cost_flat = fields.Float(string='Proofread Cost Flat Fee')